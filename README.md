# daverona/compose/logspout


## References

* Logspout repository: [https://github.com/gliderlabs/logspout](https://github.com/gliderlabs/logspout)
* Logspout registry: [https://hub.docker.com/r/gliderlabs/logspout](https://hub.docker.com/r/gliderlabs/logspout)
* [https://github.com/looplab/logspout-logstash](https://github.com/looplab/logspout-logstash)
* [https://logz.io/blog/docker-logging/](https://logz.io/blog/docker-logging/)
